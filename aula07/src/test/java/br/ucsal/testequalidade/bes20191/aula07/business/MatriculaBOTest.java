package br.ucsal.testequalidade.bes20191.aula07.business;

import org.junit.Test;

import br.ucsal.testequalidade.bes20191.aula07.builders.AlunoBuilder;
import br.ucsal.testequalidade.bes20191.aula07.builders.AlunoObjectMother;
import br.ucsal.testequalidade.bes20191.aula07.domain.Aluno;
import br.ucsal.testequalidade.bes20191.aula07.domain.SituacaoEnum;

public class MatriculaBOTest {

	@Test
	public void matricularAlunoTrancado1() {
		// Instanciar objetos aqui reduz o seu reuso.

		// F�cil de ler, dif�cil de escrever.
		Aluno aluno1 = new Aluno();
		aluno1.setNome("Maria");
		aluno1.setEndereco("Rua x");
		aluno1.setSituacao(SituacaoEnum.TRANCADO);
		aluno1.setNomeMae("Joana");
		aluno1.setMatricula(2000);
		aluno1.setAnoNascimento(1998);

		// F�cil de escrever, dif�cil de ler.
		Aluno aluno2 = new Aluno(2000, "Maria", "Joana", "Rua x", SituacaoEnum.TRANCADO, 1998);
	}

	@Test
	public void matricularAlunoTrancado2() {
		Aluno aluno1 = AlunoObjectMother.obterAlunoTrancadoMaiorIdade();
		aluno1.setNome("Pedro");
		aluno1.setNomeMae("Clara");
		aluno1.setMatricula(123);
	}

	@Test
	public void matricularAlunoTrancado3() {
		Aluno aluno1 = AlunoBuilder.umAluno().comNome("Ana").trancado().build();
	}

	@Test
	public void matricularAlunoTrancado4() {
		AlunoBuilder alunoBuilder = AlunoBuilder.umAluno().comNome("Ana")
								    .comDataNascimento(2001).trancado();
		Aluno aluno1 = alunoBuilder.mas().comDataNascimento(1970).build();
		Aluno aluno2 = alunoBuilder.mas().ativo().build();
		Aluno aluno3 = alunoBuilder.mas().comNome("Pedro").build();
	}

	@Test
	public void matricularAlunoTrancado5() {
		Aluno aluno1 = AlunoBuilder.umAlunoTrancado().build();
	}

	@Test
	public void matricularAlunoCancelado() {

	}

	@Test
	public void matricularAlunoAtivoMaior() {

	}

	@Test
	public void matricularAlunoAtivoMenor() {

	}

}
