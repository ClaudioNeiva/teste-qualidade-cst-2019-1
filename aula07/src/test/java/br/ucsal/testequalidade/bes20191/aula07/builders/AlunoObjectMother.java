package br.ucsal.testequalidade.bes20191.aula07.builders;

import br.ucsal.testequalidade.bes20191.aula07.domain.Aluno;
import br.ucsal.testequalidade.bes20191.aula07.domain.SituacaoEnum;

public class AlunoObjectMother {

	public static Aluno obterAlunoTrancadoMaiorIdade() {
		Aluno aluno = new Aluno();
		aluno.setNome("Maria");
		aluno.setEndereco("Rua x");
		aluno.setSituacao(SituacaoEnum.TRANCADO);
		aluno.setNomeMae("Joana");
		aluno.setMatricula(2000);
		aluno.setAnoNascimento(1998);
		return aluno;
	}

}
