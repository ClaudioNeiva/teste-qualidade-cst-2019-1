package br.ucsal.testequalidade.bes20191.aula07.builders;

import br.ucsal.testequalidade.bes20191.aula07.domain.Aluno;
import br.ucsal.testequalidade.bes20191.aula07.domain.SituacaoEnum;

public class AlunoBuilder {

	private static final Integer MATRICULA_DEFAULT = 1;
	private static final String NOME_DEFAULT = "Claudio";
	private static final String NOME_MAE_DEFAULT = "Maria";
	private static final String ENDERECO_DEFAULT = "Rua x";
	private static final SituacaoEnum SITUACAO_DEFAULT = null;
	private static final Integer ANO_NASCIMENTO_DEFAULT = 2000;

	private Integer matricula = MATRICULA_DEFAULT;
	private String nome = NOME_DEFAULT;
	private String nomeMae = NOME_MAE_DEFAULT;
	private String endereco = ENDERECO_DEFAULT;
	private SituacaoEnum situacao = SITUACAO_DEFAULT;
	private Integer anoNascimento = ANO_NASCIMENTO_DEFAULT;

	private AlunoBuilder() {
	}

	public static AlunoBuilder umAluno() {
		return new AlunoBuilder();
	}
	
	public static AlunoBuilder umAlunoTrancado() {
		return new AlunoBuilder().trancado();
	}
	
	public static AlunoBuilder umAlunoAtivoMaiorIdade() {
		return new AlunoBuilder().ativo().comDataNascimento(1950);
	}

	public AlunoBuilder comMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}

	public AlunoBuilder comNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder semMatricula(){
		this.matricula = null;
		return this;
	}

	public AlunoBuilder comNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
		return this;
	}

	public AlunoBuilder comEndereco(String endereco) {
		this.endereco = endereco;
		return this;
	}

	public AlunoBuilder trancado() {
		this.situacao = SituacaoEnum.TRANCADO;
		return this;
	}

	public AlunoBuilder cancelado() {
		this.situacao = SituacaoEnum.CANCELADO;
		return this;
	}

	public AlunoBuilder ativo() {
		this.situacao = SituacaoEnum.ATIVO;
		return this;
	}

	public AlunoBuilder comDataNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder mas(){
		AlunoBuilder alunoBuilder = new AlunoBuilder();
		alunoBuilder.nome = nome; 
		alunoBuilder.endereco = endereco; 
		alunoBuilder.situacao = situacao; 
		alunoBuilder.matricula = matricula; 
		alunoBuilder.anoNascimento = anoNascimento; 
		return alunoBuilder; 
	}
	
	public Aluno build() {
		Aluno aluno = new Aluno();
		aluno.setNome(nome);
		aluno.setEndereco(endereco);
		aluno.setSituacao(situacao);
		aluno.setNomeMae(nomeMae);
		aluno.setMatricula(matricula);
		aluno.setAnoNascimento(anoNascimento);
		return aluno;
	}

}
