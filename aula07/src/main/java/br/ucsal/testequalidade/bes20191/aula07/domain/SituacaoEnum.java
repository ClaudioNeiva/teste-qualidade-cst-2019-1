package br.ucsal.testequalidade.bes20191.aula07.domain;

public enum SituacaoEnum {
	ATIVO, TRANCADO, CANCELADO;
}
