package br.ucsal.testequalidade.bes20191.aula07.domain;

public class Aluno {

	private Integer matricula;

	private String nome;

	private String nomeMae;

	private String endereco;

	private SituacaoEnum situacao;

	private Integer anoNascimento;

	public Aluno() {

	}

	public Aluno(Integer matricula, String nome, String nomeMae, String endereco, SituacaoEnum situacao,
			Integer anoNascimento) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.nomeMae = nomeMae;
		this.endereco = endereco;
		this.situacao = situacao;
		this.anoNascimento = anoNascimento;
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeMae() {
		return nomeMae;
	}

	public void setNomeMae(String nomeMae) {
		this.nomeMae = nomeMae;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public SituacaoEnum getSituacao() {
		return situacao;
	}

	public void setSituacao(SituacaoEnum situacao) {
		this.situacao = situacao;
	}

	public Integer getAnoNascimento() {
		return anoNascimento;
	}

	public void setAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
	}

}
