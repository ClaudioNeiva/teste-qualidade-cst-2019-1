package br.ucsal.bes20191.testequalidade.aula14;

public class CalculoUtil {

	public static Long calcularFatorial(Integer n) {
		if (n == 0) {
			return 1L;
		}
		return n * calcularFatorial(n - 1);
	}

}
