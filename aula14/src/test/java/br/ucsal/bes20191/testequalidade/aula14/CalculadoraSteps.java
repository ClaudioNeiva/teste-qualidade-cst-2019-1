package br.ucsal.bes20191.testequalidade.aula14;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CalculadoraSteps {
	
	private Aluno aluno;

	@Given("um aluno est� matriculado na disciplina")
	public void instanciarAluno() {
		aluno = new Aluno();
	}

	@When("informo a nota $nota")
	public void informarNota(Double nota) {
		aluno.informarNota(nota);
	}

	@Then("a situa��o do aluno � $situacao")
	public void verificarSituacaoAluno(String situacao) {
		Assert.assertEquals(situacao, aluno.obterSituacao());
	}


}
