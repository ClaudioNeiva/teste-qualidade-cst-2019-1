package br.ucsal.bes20191.testequalidade.aula14;

import org.junit.Assert;
import org.junit.Test;

public class CalculoTest {

	@Test
	public void testarFatorial5() {
		// Entrada
		Integer n = 5;

		// Sa�da esperada
		Long fatorialEsperado = 120L;

		// Chamar o m�todo que est� sendo testado e armazenar o resultado
		// esperado
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
