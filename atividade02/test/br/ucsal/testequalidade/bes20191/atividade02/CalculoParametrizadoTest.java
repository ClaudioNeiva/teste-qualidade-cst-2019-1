package br.ucsal.testequalidade.bes20191.atividade02;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalculoParametrizadoTest {

	@Parameters(name="calcularFatorial({0})")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { 0, 1L }, { 1, 1L }, { 2, 2L }, { 3, 6L }, { 4, 24L }, { 5, 120L },
				{ 6, 720L }, { 7, 5040L }, { 8, 40320L }, { 9, 362880L } });
	}

	@Parameter // Default (0)
	public Integer n;

	@Parameter(1)
	public Long fatorialEsperado;
	
	@Test
	public void testarFatorial() {
		// Setup

		// Executar o m�todo sob teste e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);

		// Comparar o resultado esperado como resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);

		// Teardown
	}

}
