package br.ucsal.testequalidade.bes20191.atividade02;

import org.junit.Assert;
import org.junit.Test;

public class CalculoTest {

	@Test
	public void testarFatorial6() {
		// Setup

		// Definir dados de entrada
		Integer n = 6;

		// Definir sa�da esperada
		Long fatorialEsperado = 720L;

		// Executar o m�todo sob teste e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);

		// Comparar o resultado esperado como resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
		
		//Teardown
	}

	// Pesquisar o uso do Parameters do JUnit para evitar essa redun�ncia de
	// c�digo.
	@Test
	public void testarFatorial0() {
		// Setup

		// Definir dados de entrada
		Integer n = 0;

		// Definir sa�da esperada
		Long fatorialEsperado = 1L;

		// Executar o m�todo sob teste e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);

		// Comparar o resultado esperado como resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
		
		//Teardown
	}

}
