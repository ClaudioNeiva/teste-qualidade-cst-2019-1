package br.ucsal.bes20191.testequalidade.restaurante.poolista01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.CalculosUtil;
import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class CalculosUtilIntegradoTest {

	private CalculosUtil calculosUtil;

	@Before
	public void setup() {
		FatorialUtil fatorialUtil = new FatorialUtil();
		calculosUtil = new CalculosUtil(fatorialUtil);
	}

	/**
	 * Testar c�lculo do valor de E: Caso de teste1: n=2 -> E=2.5
	 */
	@Test
	public void testarCalculoE2() {
		// Defini��o dos dados de entrada
		Integer n = 2;

		// Defini��o do resultado esperado
		Double eEsperado = 2.5;

		// Execu��o do m�todo sob teste e obten��o do resultado atual
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.0001);
	}
}
