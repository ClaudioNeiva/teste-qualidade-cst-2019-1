package br.ucsal.bes20191.testequalidade.restaurante.poolista01.commockito;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.CalculosUtil;
import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class CalculosUtilUnitarioTest {

	private CalculosUtil calculosUtil;

	private FatorialUtil fatorialUtilMock;

	@Before
	public void setup() {
		fatorialUtilMock = Mockito.mock(FatorialUtil.class);

		Mockito.when(fatorialUtilMock.calcularFatorial(0)).thenReturn(1L);
		Mockito.when(fatorialUtilMock.calcularFatorial(1)).thenReturn(1L);
		Mockito.when(fatorialUtilMock.calcularFatorial(2)).thenReturn(2L);
		Mockito.when(fatorialUtilMock.calcularFatorial(3)).thenReturn(6L);

		calculosUtil = new CalculosUtil(fatorialUtilMock);
	}

	/**
	 * Testar c�lculo do valor de E: Caso de teste1: n=2 -> E=2.5
	 */
	@Test
	public void testarCalculoE2() {
		// Defini��o dos dados de entrada
		Integer n = 2;

		// Defini��o do resultado esperado
		Double eEsperado = 2.5;

		// Execu��o do m�todo sob teste e obten��o do resultado atual
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.0001);

		// O m�todo calcularE(n) � do tipo query, e portanto, o seu retorno �
		// suficiente para comprovar sua execu��o com sucesso.
		// A verifica��o a seguir esta sendo feita apenas para ilustrar como
		// seria o uso de um Mock.
		Mockito.verify(fatorialUtilMock).calcularFatorial(0);
		Mockito.verify(fatorialUtilMock).calcularFatorial(1);
		Mockito.verify(fatorialUtilMock).calcularFatorial(2);
	}

	/**
	 * Testar c�lculo do valor de E: Caso de teste2: n=3 -> E=2.66
	 */
	@Test
	public void testarCalculoE3() {
		// Defini��o dos dados de entrada
		Integer n = 3;

		// Defini��o do resultado esperado
		Double eEsperado = 2.66;

		// Execu��o do m�todo sob teste e obten��o do resultado atual
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
