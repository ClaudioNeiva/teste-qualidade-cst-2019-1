package br.ucsal.bes20191.testequalidade.restaurante.poolista01.solucaopersonalizada;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.CalculosUtil;
import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class CalculosUtilUnitarioTest {

	private CalculosUtil calculosUtil;

	private FatorialUtil fatorialUtilMock;

	@Before
	public void setup() {
		// FatorialUtil fatorialUtil = new FatorialUtilStub();
		fatorialUtilMock = new FatorialUtilMock();
		calculosUtil = new CalculosUtil(fatorialUtilMock);
	}

	/**
	 * Testar c�lculo do valor de E: Caso de teste1: n=2 -> E=2.5
	 */
	@Test
	public void testarCalculoE2() {
		// Defini��o dos dados de entrada
		Integer n = 2;

		// Defini��o do resultado esperado
		Double eEsperado = 2.5;

		// Execu��o do m�todo sob teste e obten��o do resultado atual
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.0001);

		// O m�todo calcularE(n) � do tipo query, e portanto, o seu retorno �
		// suficiente para comprovar sua execu��o com sucesso.
		// A verifica��o a seguir esta sendo feita apenas para ilustrar como
		// seria o uso de um Mock.
		((FatorialUtilMock) fatorialUtilMock).verificar("calcularFatorial", 0, 1);
		((FatorialUtilMock) fatorialUtilMock).verificar("calcularFatorial", 1, 1);
		((FatorialUtilMock) fatorialUtilMock).verificar("calcularFatorial", 2, 1);
	}

	/**
	 * Testar c�lculo do valor de E: Caso de teste2: n=3 -> E=2.66
	 */
	@Test
	public void testarCalculoE3() {
		// Defini��o dos dados de entrada
		Integer n = 3;

		// Defini��o do resultado esperado
		Double eEsperado = 2.66;

		// Execu��o do m�todo sob teste e obten��o do resultado atual
		Double eAtual = calculosUtil.calcularE(n);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(eEsperado, eAtual, 0.01);
	}

}
