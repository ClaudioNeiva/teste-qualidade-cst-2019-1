package br.ucsal.bes20191.testequalidade.restaurante.poolista01.solucaopersonalizada;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExemploAnotacoesJUnit {

	@BeforeClass
	public static void setupClasse() {
		System.out.println("setupClasse");
	}

	@Before
	public void setup() {
		System.out.println("\nsetup");
	}

	@Test
	public void teste1() {
		System.out.println("teste1");
	}

	@Test
	public void teste2() {
		System.out.println("teste2");
	}

	@Test
	public void teste3() {
		System.out.println("teste3");
	}

	@After
	public void teardown() {
		System.out.println("teardown\n");
	}

	@AfterClass
	public static void teardownClasse() {
		System.out.println("teardownClasse");
	}

}
