package br.ucsal.bes20191.testequalidade.restaurante.poolista01.solucaopersonalizada;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class FatorialUtilStub extends FatorialUtil {

	@Override
	public Long calcularFatorial(int n) {
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		case 3:
			return 6L;
		}
		return 0L;
	}

}
