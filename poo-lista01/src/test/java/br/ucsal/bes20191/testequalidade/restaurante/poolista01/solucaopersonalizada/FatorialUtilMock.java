package br.ucsal.bes20191.testequalidade.restaurante.poolista01.solucaopersonalizada;

import java.util.HashMap;
import java.util.Map;

import br.ucsal.bes20191.testequalidade.restaurante.poolista01.FatorialUtil;

public class FatorialUtilMock extends FatorialUtil {

	// Map<identificacao-chamada, qtd-ocorrencias-chamada>
	private Map<String, Integer> chamadas = new HashMap<>();

	@Override
	public Long calcularFatorial(int n) {
		registrar("calcularFatorial", n);
		switch (n) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		case 3:
			return 6L;
		}
		return 0L;
	}

	private void registrar(String metodo, int parametro) {
		String identificador = gerarIdentificador(metodo, parametro);
		if (chamadas.containsKey(identificador)) {
			chamadas.put(identificador, chamadas.get(identificador) + 1);
		} else {
			chamadas.put(identificador, 1);
		}
	}

	private String gerarIdentificador(String metodo, int parametro) {
		return metodo + "(" + parametro + ")";
	}

	public void verificar(String metodo, Integer parametro, Integer qtdChamadas) {
		String identificador = gerarIdentificador(metodo, parametro);
		if (!chamadas.containsKey(identificador) || chamadas.get(identificador) != qtdChamadas) {
			throw new RuntimeException("Chamada ao m�todo " + identificador + " n�o ocorreu " + qtdChamadas);
		}
	}

}
