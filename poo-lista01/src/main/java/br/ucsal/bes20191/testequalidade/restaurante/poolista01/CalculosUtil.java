package br.ucsal.bes20191.testequalidade.restaurante.poolista01;

public class CalculosUtil {

	public FatorialUtil fatorialUtil;

	public CalculosUtil(FatorialUtil fatorialUtil) {
		this.fatorialUtil = fatorialUtil;
	}

	/**
	 * Calculo do valor de E(n) (soma dos inversos dos fatoriais 
	 * de 0 a n):
	 * 
	 * E = 1 + 1 / 1! + 1 / 2! + 1 / 3! + ... + 1 / N!
	 * 
	 * @param n
	 *            - valor sobre o qual ser� calculado o valor de E
	 * @return valor de E
	 */
	public Double calcularE(Integer n) {
		Double e = 0d;
		for (int i = 0; i <= n; i++) {
			e += 1d / fatorialUtil.calcularFatorial(i);
		}
		return e;
	}

}
