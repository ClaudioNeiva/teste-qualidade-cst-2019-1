package br.ucsal.testequalidade.bes20191.atividade02;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class CalculoEntradaSaidaTest {

	private static final String QUEBRA_LINHA = System.getProperty("line.separator");

	@Test
	public void testarExibicaoFatorial5() {
		// Setup
		ByteArrayOutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		// Dados de entrada
		Integer n = 5;
		Long fatorial = 120L;

		// Sa�da esperada
		String mensagemEsperada = "Fatorial(5)=120" + QUEBRA_LINHA;

		// Executar o m�todo a ser testado e obter o resultado atual
		Calculo.exibirFatorial(n, fatorial);
		String mensagemAtual = outFake.toString();

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(mensagemEsperada, mensagemAtual);
	}

}
