package br.ucsal.testequalidade.bes20191.atividade02;


//NUNCA FA�A ASSIM!!!
public class CalculoSemJunitTest {

	public static void main(String[] args) {
		CalculoSemJunitTest calculoTest = new CalculoSemJunitTest();
		calculoTest.testarFatorial6();
	}

	public void testarFatorial6() {
		// Setup
		
		// Definir dados de entrada
		Integer n = 6;
		
		// Definir a sa�da esperada
		Long fatorialEsperado = 720L;
		
		// Executar o m�todo que est� sob teste e obter o resultado atual
		Long fatorialAtual = Calculo.calcularFatorial(n);
		
		// Comparar o resultado esperado com o resultado atual
		if (fatorialEsperado.equals(fatorialAtual)) {
			System.out.println("testarFatorial5 executado com sucesso.");
		} else {
			System.out.println(
					"erro ao executar testarFatorial5. Esperado=" + fatorialEsperado + "; Atual=" + fatorialAtual);
		}
	}

}
