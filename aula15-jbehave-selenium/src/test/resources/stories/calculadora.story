Cenário: Cálculo da média para aluno aprovado
Dado que estou na calculadora
Quando informo 8 para a nota1
E informo 6 para a nota2
Então é apresentada a média 7

Cenário: Cálculo da média para aluno reprovado
Dado que estou na calculadora
Quando informo 3 para a nota1
E informo 2 para a nota2
Então é apresentada a média 2.5