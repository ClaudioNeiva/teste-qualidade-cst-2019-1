package br.ucsal.bes20191.testequalidade.aula15;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CalculadoraSteps {

	private WebDriver driver;

	public CalculadoraSteps(WebDriver driver) {
		this.driver = driver;
	}
	
	@Given("estou na calculadora")
	public void abrirCalculadora() {
		driver.get(
				"file:///C:/Users/antoniocp.CEGEN/Documents/2019-1/teste-qualidade/workspace/aula15-jbehave-selenium/src/main/resources/webapp/calculadora.html");
	}

	@When("informo $valorNota para a $campoNota")
	public void informarNota(String valorNota, String campoNota) {
		WebElement nota = driver.findElement(By.id(campoNota));
		nota.sendKeys(valorNota);
	}

	@Then("é apresentada a média $valorMedia")
	public void verificarMedia(String valorMediaEsperada) {
		WebElement calcularMedia = driver.findElement(By.id("calcularMediaBtn"));
		calcularMedia.click();
		WebElement media = driver.findElement(By.id("media"));
		String valorMediaAtual = media.getAttribute("value");
		Assert.assertEquals(valorMediaEsperada, valorMediaAtual);
	}

}
